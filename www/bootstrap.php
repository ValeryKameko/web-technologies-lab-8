<?php

include_once __DIR__ . '/src/Autoloader.php';

Autoloader::setBaseDirectory(__DIR__ . '/src');
Autoloader::addPrefix('TemplateEngine', 'TemplateEngine');
Autoloader::addPrefix('Model', 'models');
Autoloader::addPrefix('Controller', 'controllers');
Autoloader::addPrefix('View', 'views');
Autoloader::addPrefix('Database', 'databases');
Autoloader::addPrefix('', '');
Autoloader::register();

$router = new Router();

$router->addRoute('/^\/?$/', function(...$routeParams) {
    Controller\Controller::redirect('/history');
});

$router->addRoute('/^\/?info$/', function(...$routeParams) {
    phpinfo();
});

$router->addRoute('/^\/?page.*$/', function(...$routeParams) {
    $controller = new Controller\PageController();
    $controller->handle($_GET);
});

$router->addRoute('/^\/?image_statistics.*$/', function(...$routeParams) {
    $controller = new Controller\ImageStatisticsController();
    $controller->handle($_GET);
});

$router->addRoute('/^\/?main_page.*$/', function(...$routeParams) {
    $controller = new Controller\MainPageController();
    $controller->handle($_GET);
});

$router->addRoute('/^\/?clear.*$/', function(...$routeParams) {
    $controller = new Controller\ClearController();
    $controller->handle($_GET);
});

$router->route();