<?php

namespace View;

class PageView extends View {
    private $basePath;

    public function __construct($basePath) {
        parent::__construct();
        $this->basePath = $basePath;
    }

    public function display($parameters) {
        $template = $this->templateEngineEnvironment->load($this->basePath);
        $template->display($parameters);
    }

    public function getBasePath() {
        return $this->basePath;
    }
}
