<?php

namespace View;

use TemplateEngine\Environment;
use TemplateEngine\FilesystemTemplateLoader;

abstract class View {
    protected $templateEngineEnvironment;

    public function __construct() {
        $this->templateEngineEnvironment = new Environment(
            new FilesystemTemplateLoader(dirname(dirname(__DIR__))));
    }

    public abstract function display($parameters);

    public function render($parameters) {
        \ob_start();
        $this->display($parameters);
        return \ob_get_clean();
    }
}