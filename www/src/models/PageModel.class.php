<?php

namespace Model;

class PageModel {
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function getPage($id) {
        $sql = 'SELECT *
                FROM `site`.`page`
                WHERE `page`.`id` = :id';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':id' => $id
        ]);

        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    public function addPage(
            $title,
            $content_path,
            $header_path,
            $footer_path,
            $configs_path,
            $style_uri,
            $script_uri) {
        $sql = 'INSERT INTO `site`.`page` (
                    `title`,
                    `content_path`,
                    `header_path`,
                    `footer_path`,
                    `configs_path`,
                    `style_uri`,
                    `script_uri`,
                )
                VALUE (
                    :title,
                    :content_path,
                    :header_path,
                    :footer_path,
                    :configs_path,
                    :style_uri,
                    :script_uri,
                    )';
        $query = $this->db->prepare($sql);
        return $query->execute([
            ':title'            => $title,
            ':content_path'     => $content_path,
            ':header_path'      => $header_path,
            ':footer_path'      => $footer_path,
            ':configs_path'     => $configs_path,
            ':style_uri'        => $style_uri,
            ':script_uri'       => $script_uri,
        ]);
    }
    
    public function getLinks($id) {
        $sql = 'SELECT *
                FROM `site`.`link`
                WHERE `link`.`page_id` = :id
                ORDER BY `link`.`num` ASC';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':id' => $id
        ]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getAllPages() {
        $sql = 'SELECT *
                FROM `site`.`page`';
        $query = $this->db->prepare($sql);
        $query->execute([]);

        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getAllPagesExcept($id) {
        $sql = 'SELECT *
                FROM `site`.`page`
                WHERE `page`.`id` <> :id';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':id' => $id
        ]);

        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function addLink($link, $name, $num) {
        $sql = 'INSERT INTO `site`.`links` (
                    `link`,
                    `num`,
                    `name`
                )
                VALUE (:link, :page_id, :num)';
        $query = $this->db->prepare($sql);
        return $query->execute([
            ':link' => $link,
            ':num' => $num,
            ':name' => $name,
        ]);
    }
} 
