<?php

namespace Model;

class ProcessImagesModel {
    public function __construct() {
    }

    private function processImage($match, $resolver) {
        $attributes = explode(' ', $match[1]);
        $srcAttribute = array_values(array_filter($attributes, function($element) {
            return preg_match('/^src="(.*)"$/', $element);
        }))[0] ?? null;

        preg_match('/^src="(.*)"$/', $srcAttribute, $matches);
        $srcLink = $matches[1];
        $id = $resolver($srcLink);
        return '<img src="/image_statistics?id=' . $id . '" width="0" height="0">' . $match[0];
    }

    public function process($html, $resolver) {
        return preg_replace_callback('/<img([^>]*)>/', function ($match) use($resolver) {
            return $this->processImage($match, $resolver);
        }, $html);
    }
}