<?php

namespace Model;

class ImageStatisticsModel {
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function getAllStatistics() {
        $sql = 'SELECT * 
                FROM `site`.`image_statistics`';
        $query = $this->db->prepare($sql);
        $query->execute([]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function addImageUrl($url) {
        $sql = 'INSERT IGNORE INTO `site`.`image_statistics` (
                    `url`,
                    `count`
                )
                VALUE (:url, 0)';
        $query = $this->db->prepare($sql);
        return $query->execute([
            ':url' => $url
        ]);
    }

    public function getEntryById($id) {
        $sql = 'SELECT * 
                FROM `site`.`image_statistics`
                WHERE `image_statistics`.`id` = :id';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':id' => $id
        ]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getEntryByUrl($url) {
        $sql = 'SELECT * 
                FROM `site`.`image_statistics`
                WHERE `image_statistics`.`url` = :url';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':url' => $url
        ]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function countImageUrl($url) {
        $sql = 'SELECT COUNT(*) 
                FROM `size`.`image_statistics`
                WHERE `image_statistics`.`url` = :url';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':url' => $url
        ]);
        return $query->fetch();
    }

    public function updateImageStatistics($id) {
        $sql = 'UPDATE `site`.`image_statistics`
                SET `image_statistics`.`count` = `image_statistics`.`count` + 1
                WHERE `image_statistics`.`id` = :id';
        $query = $this->db->prepare($sql);
        return $query->execute([
            ':id' => $id
        ]);
    }

    public function clear() {
        $sql = 'DELETE
                FROM `site`.`image_statistics`';
        $query = $this->db->prepare($sql);
        return $query->execute([]);        
    }
}
