<?php

namespace TemplateEngine\NodeParser;

use TemplateEngine\Parser;
use TemplateEngine\Token;
use TemplateEngine\Node\RawIncludeBlockNode;

class RawIncludeBlockNodeParser extends AbstractNodeParser
{
    public function __construct()
    {

    }

    public function subparse(Parser $parser)
    {
        $line = $parser->getLine();
        $parser->expect(Token::NAME_TYPE, 'raw_include');
        $rawIncludeFilePathExpression = $parser->parseExpression();
        return new RawIncludeBlockNode($rawIncludeFilePathExpression, $line);
    }
}