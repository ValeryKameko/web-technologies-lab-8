<?php

namespace TemplateEngine\Node\Expression;

use TemplateEngine\Error\UnimlementedError;
use TemplateEngine\Compiler;

class ConstantExpressionNode extends AbstractExpressionNode
{
    public function __construct($value, $line)
    {
        parent::__construct([], [ 'value' => $value ], $line, 'constant');
    }

    public function compile(Compiler $compiler)
    {
        if (\is_null($this->attributes['value']))
            $compiler->write('NULL');
        elseif (\is_bool($this->attributes['value']))
            $compiler->write($this->attributes['value'] ? 'true' : 'false');
        elseif (\is_string($this->attributes['value']))
            $compiler->write('\'' . $this->attributes['value'] . '\'');
        elseif (\is_numeric($this->attributes['value']))
            $compiler->write(\strval($this->attributes['value']));
    }
}
