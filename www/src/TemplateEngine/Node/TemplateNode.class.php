<?php

namespace TemplateEngine\Node;

use TemplateEngine\Compiler;
use TemplateEngine\Error\UnimlementedError;

class TemplateNode extends Node 
{
    public function __construct($body)
    {
        parent::__construct(['body' => $body], [], NULL, 'template');
    }

    public function compile(Compiler $compiler)
    {
        $this->nodes['body']->compile($compiler);
    }
}