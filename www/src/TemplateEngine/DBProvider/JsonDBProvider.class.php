<?php

namespace TemplateEngine\DBProvider;

use TemplateEngine\DBProvider;

class JsonDBProvider implements DBProvider
{
    private $data;

    public function __construct($filePath) {
        $jsonData = \file_get_contents($filePath);
        $this->data = \json_decode($jsonData, true);
    }

    public function get($variable) {
        return $this->data[$variable];
    }
}