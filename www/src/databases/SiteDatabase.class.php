<?php

namespace Database;

class SiteDatabase extends Database {
    public static $self;

    protected function __construct() {
        parent::__construct('mysql-server', '3306', 'user', 'passwd', 'site');
    }

    public static function getDatabase() {
        if (!isset($self))
            $self = new SiteDatabase();
        return $self;
    }
}

