<?php

namespace Database;

use \PDO;

abstract class Database {
    private $db;

    protected function __construct($host, $port, $user, $password, $dbname) {
        $dsn = "mysql:host=$host;port=$port;dbname=$dbname";
        $options = array(
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        );
        $this->db = new \PDO($dsn, $user, $password, $options);
    }

    public function getPDO() {
        return $this->db;
    }
}