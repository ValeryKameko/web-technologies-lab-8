<?php

namespace Controller;

use View\PageView;
use Model\PageModel;
use Model\ProcessImagesModel;
use Model\ImageStatisticsModel;
use Database\SiteDatabase;

class ClearController {
    private $db;
    private $pageModel;
    private $pageView;
    private $processImageModel;
    private $imageStatisticsModel;
    public function __construct() {
        $this->db = SiteDatabase::getDatabase();
        $this->pageModel = new PageModel($this->db->getPDO());
        $this->processImageModel = new ProcessImagesModel();
        $this->imageStatisticsModel = new ImageStatisticsModel($this->db->getPDO());
    }

    public function handle($options) {
        $backUrl = $options['back_url'] ?? null;
        if (empty($backUrl))
            Controller::redirect($backUrl);
        
        $this->imageStatisticsModel->clear();

        Controller::redirect($backUrl);
    }
}
