<?php

namespace Controller;

use Model\ImageStatisticsModel;
use Database\SiteDatabase;

class ImageStatisticsController {
    private $db;
    private $imageStatisticsModel;

    public function __construct() {
        $this->db = SiteDatabase::getDatabase();
        $this->imageStatisticsModel = new ImageStatisticsModel($this->db->getPDO());
    }

    public function handle($options) {
        $id = filter_var($options['id'] ?? null, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
        if (is_null($id))
            Controller::notFound();
        
        $result = $this->imageStatisticsModel->updateImageStatistics($id);
        if (!$result)
            Controller::notFound();
    }
}
