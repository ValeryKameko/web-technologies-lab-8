<?php

namespace Controller;

use View\PageView;
use Model\PageModel;
use Model\ProcessImagesModel;
use Model\ImageStatisticsModel;
use Database\SiteDatabase;

class MainPageController {
    private $db;
    private $pageModel;
    private $pageView;
    private $processImageModel;
    private $imageStatisticsModel;
    public function __construct() {
        $this->db = SiteDatabase::getDatabase();
        $this->pageModel = new PageModel($this->db->getPDO());
        $this->processImageModel = new ProcessImagesModel();
        $this->imageStatisticsModel = new ImageStatisticsModel($this->db->getPDO());
    }

    public function handle($options) {
        $id = 0;
        $page = $this->pageModel->getPage($id);
        $this->pageView = new PageView($page['base_path']);

        $links = $this->pageModel->getLinks($id);
        $pages = $this->pageModel->getAllPagesExcept(0);
        $links = array_merge($links, array_map(function ($page) {
            return [
                'link' => '/page?id=' . $page['id'],
                'name' => 'Page ' . $page['id']
            ];
        }, $pages), [
            [
                'link' => '/clear?back_url=' . urlencode('/main_page'), 
                'name' => 'Clear'
            ]
        ]);
        $entries = $this->imageStatisticsModel->getAllStatistics();

        $render = $this->pageView->render([
            'page' => $page,
            'links' => $links,
            'entries' => $entries
        ]);
        echo $render;
    }
}
