<?php

namespace Controller;

use View\PageView;
use Model\PageModel;
use Model\ProcessImagesModel;
use Model\ImageStatisticsModel;
use Database\SiteDatabase;

class PageController {
    private $db;
    private $pageModel;
    private $pageView;
    private $processImageModel;
    private $imageStatisticsModel;

    public function __construct() {
        $this->db = SiteDatabase::getDatabase();
        $this->pageModel = new PageModel($this->db->getPDO());
        $this->processImageModel = new ProcessImagesModel();
        $this->imageStatisticsModel = new ImageStatisticsModel($this->db->getPDO());
    }

    public function resolveImageUrl($url) {
        $entries = $this->imageStatisticsModel->getEntryByUrl($url);
        if (empty($entries)) {
            $this->imageStatisticsModel->addImageUrl($url);
            $entries = $this->imageStatisticsModel->getEntryByUrl($url);
        }
        return $entries[0]['id'];
    }

    public function handle($options) {
        $id = $options['id'] ?? null;
        if (is_null($id))
            Controller::notFound();

        $page = $this->pageModel->getPage($id);
        if (!$page)
            Controller::notFound();
        $this->pageView = new PageView($page['base_path']);

        $links = $this->pageModel->getLinks($id);
        $links = array_merge($links, [
            [
                'link' => '/main_page',
                'name' => 'To Main'
            ]
        ]);

        $render = $this->pageView->render([
            'page' => $page,
            'links' => $links
        ]);
        $render = $this->processImageModel->process($render, [$this, 'resolveImageUrl']);
        echo $render;
    }
}
