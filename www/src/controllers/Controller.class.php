<?php

namespace Controller;

class Controller {
    public function __construct() {

    }

    public static function redirect($url) {
        header("Location: $url");
        exit;
    }

    public static function notFound() {
        http_response_code(404);
        exit;
    }
}