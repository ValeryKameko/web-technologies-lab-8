CREATE DATABASE IF NOT EXISTS `site` COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `site`.`page` (
    `id` int NOT NULL AUTO_INCREMENT,
    `title` tinytext NOT NULL,
    `base_path` tinytext NOT NULL,
    `content_path` tinytext NOT NULL,
    `header_path` tinytext NOT NULL,
    `footer_path` tinytext NOT NULL,
    `style_uri` tinytext NOT NULL,
    `script_uri` tinytext,
    PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `site`.`link` (
    `id` int NOT NULL AUTO_INCREMENT,
    `link` tinytext NOT NULL,
    `name` tinytext NOT NULL,
    PRIMARY KEY (`id`),
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `site`.`image_statistics` (
    `id` int NOT NULL AUTO_INCREMENT,
    `url` tinytext NOT NULL,
    `count` int NOT NULL,
    PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;